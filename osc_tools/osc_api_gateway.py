"""This module is used as a gateway to the OSC api."""
import asyncio
import concurrent.futures
import os.path
import shutil
import logging
from enum import Enum
import requests
from requests import RequestException

LOGGER = logging.getLogger('osc_tools.osc_api_gateway')


def _upload_url(env: str, resource: str) -> str:
    return _osc_url(env) + '/' + resource


def _osc_url(env: str) -> str:
    base_url = __protocol() + env + __domain()
    return base_url


def __protocol() -> str:
    return 'https:'


def __domain() -> str:
    return 'openstreetcam.org'


def _version() -> str:
    return '1.0'


class OSCAPIEnvironment(Enum):
    """This is a enumeration of environments"""
    TESTING = '//testing-api.'
    STAGING = '//staging-api.'
    BETA = '//beta-api.'
    PRODUCTION = '//api.'


class OSCApiMethods:
    """This is a factory class that creates API methods based on environment"""

    @classmethod
    def sequence_details(cls, env: str) -> str:
        """this method will return the link to the sequence details method"""
        return _osc_url(env) + "/details"

    @classmethod
    def user_sequences(cls, env: str) -> str:
        """this method returns the urls to the list of sequences that
        belong to a user"""
        return _osc_url(env) + "/my-list"

    @classmethod
    def resource(cls, env: str, resource_name: str) -> str:
        """this method returns the url to a resource"""
        return _osc_url(env) + '/' + resource_name

    @classmethod
    def photo_list(cls, env: str) -> str:
        """this method returns photo list URL"""
        return _osc_url(env) + '/' + _version() + '/sequence/photo-list/'

    @classmethod
    def video_upload(cls, env: str) -> str:
        """this method returns video upload URL"""
        return _upload_url(env, 'video')

    @classmethod
    def photo_upload(cls, env: str) -> str:
        """this method returns photo upload URL"""
        return _upload_url(env, 'photo')

    @classmethod
    def sequence(cls, env: str) -> str:
        """this method returns sequence URL"""
        return _osc_url(env).format('sequence')

    @classmethod
    def login(cls, env: str, provider: str) -> str:
        """this method returns login URL"""
        if provider == "osm":
            return _osc_url(env) + '/auth/openstreetmap/client_auth'
        if provider == "google":
            return _osc_url(env) + '/auth/google/client_auth'
        if provider == "facebook":
            return _osc_url(env) + '/auth/facebook/client_auth'
        return None


class OSCUser:
    """This class is model for OSC User"""

    def __init__(self):
        self.name = ""
        self.user_id = ""
        self.full_name = ""
        self.access_token = ""

    def description(self) -> str:
        """description method that will return a string representation for this class"""
        return "{ name: " + self.name + \
               ", user_id: " + self.user_id + \
               ", full_name: " + self.full_name + \
               ", access_token = " + self.access_token + " }"

    def __eq__(self, other):
        if isinstance(other, OSCUser):
            return self.user_id == other.user_id
        return False


class OSCPhoto:
    """this is a model class for a photo from OSC API"""

    def __init__(self):
        self.latitude = None
        self.longitude = None
        self.sequence_index = None
        self.photo_id = None
        self.image_name = None
        self.thumbnail_name = None
        self.date_added = None

    @classmethod
    def photo_from_json(cls, json):
        """factory method to build a photo from a json representation"""
        photo = OSCPhoto()
        if 'lat' in json:
            photo.latitude = json['lat']
        if 'lon' in json:
            photo.longitude = json['lon']
        if 'sequence_index' in json:
            photo.sequence_index = json['sequence_index']
        if 'id' in json:
            photo.photo_id = json['id']
        if 'name' in json:
            photo.image_name = json['name']
        if 'th_name' in json:
            photo.thumbnail_name = json['th_name']
        if 'date_added' in json:
            photo.date_added = json['date_added']

        return photo

    def __eq__(self, other):
        if isinstance(other, OSCPhoto):
            return self.photo_id == other.photo_id
        return False


class OSCSequence:
    """this is a model class for a sequence from OSC API"""

    def __init__(self):
        self.photos: [OSCPhoto] = []
        self.local_id: str = None
        self.online_id: str = None
        self.path: str = None
        self.metadata_url = None

    @classmethod
    def sequence_from_json(cls, json):
        """factory method to build a sequence form json representation"""
        sequence = OSCSequence()
        if 'id' in json:
            sequence.online_id = json['id']
        if 'meta_data_filename' in json:
            sequence.metadata_url = json['meta_data_filename']
        if 'photos' in json:
            photos = []
            photos_json = json['photos']
            for photo_json in photos_json:
                photo = OSCPhoto.photo_from_json(photo_json)
                photos.append(photo)
            sequence.photos = photos

        return sequence

    def __eq__(self, other):
        if isinstance(other, OSCSequence):
            return self.local_id == other.local_id and self.online_id == other.online_is
        return False


class OSCApi:
    """This class is a gateway for the API"""

    def __init__(self, env: OSCAPIEnvironment):
        self.environment = env

    def authorized_user(self, provider: str, token: str, secret: str) -> (OSCUser, Exception):
        """This method will get a authorization token for OSC API"""
        try:
            data_access = {'request_token': token,
                           'secret_token': secret
                           }
            login_url = OSCApiMethods.login(self.environment.value, provider)
            response = requests.post(url=login_url, data=data_access)
            json_response = response.json()

            if 'osv' in json_response:
                osc_data = json_response['osv']
                user = OSCUser()
                missing_field = None
                if 'access_token' in osc_data:
                    user.access_token = osc_data['access_token']
                else:
                    missing_field = "access token"

                if 'id' in osc_data:
                    user.user_id = osc_data['id']
                else:
                    missing_field = "id"

                if 'username' in osc_data:
                    user.name = osc_data['username']
                else:
                    missing_field = "username"

                if 'full_name' in osc_data:
                    user.full_name = osc_data['full_name']
                else:
                    missing_field = "fullname"

                if missing_field is not None:
                    return None, Exception("OSC API bug. OSCUser missing " + missing_field)

            else:
                return None, Exception("OSC API bug. OSCUser missing username")

        except RequestException as ex:
            return None, ex

        return user, None

    def upload_video(self):
        """This method will upload a video to OSC API"""
        print('nothing here' + self.environment.value)

    def get_photos(self, sequence_id: int) -> ([OSCPhoto], Exception):
        """this method will return a list of photo objects for a sequence id"""
        try:
            parameters = {'sequenceId': sequence_id}
            login_url = OSCApiMethods.photo_list(self.environment.value)
            response = requests.post(url=login_url, data=parameters)
            json_response = response.json()
            missing_field = None
            if 'osv' not in json_response:
                missing_field = "osv"
            elif 'photos' not in json_response['osv']:
                missing_field = "photos"
            else:
                photos = []
                photos_json = json_response['osv']['photos']
                for photo_json in photos_json:
                    photo = OSCPhoto.photo_from_json(photo_json)
                    photos.append(photo)

                return photos, missing_field
        except RequestException as ex:
            return [], ex

        return [], Exception("OSC API bug. OSCPhoto missing field:" + missing_field)

    def download_all_images(self, photo_list: [OSCPhoto],
                            track_path: str,
                            override=False,
                            workers: int = 10):
        """This method will download all images to a path overriding or not the files at
        that path. By default this method uses 10 parallel workers."""
        with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
            loop = asyncio.new_event_loop()
            futures = [
                loop.run_in_executor(executor,
                                     self.get_image, photo, track_path, override)
                for photo in photo_list
            ]
            if not futures:
                loop.close()
                return

            loop.run_until_complete(asyncio.gather(*futures))
            loop.close()

    def get_image(self, photo: OSCPhoto, path: str, override=False) -> Exception:
        """downloads the image at the path specified"""
        jpg_name = path + '/' + str(photo.sequence_index) + '.jpg'
        if not override and os.path.isfile(jpg_name):
            return None

        try:
            response = requests.get(OSCApiMethods.resource(self.environment.value,
                                                           photo.image_name),
                                    stream=True)
            if response.status_code == 200:
                with open(jpg_name, 'wb') as file:
                    response.raw.decode_content = True
                    shutil.copyfileobj(response.raw, file)
        except RequestException as ex:
            return ex

        return None

    def _sequence_page(self, user_name, page) -> ([OSCSequence], Exception):
        try:
            parameters = {'ipp': 100,
                          'page': page,
                          'username': user_name}
            login_url = OSCApiMethods.user_sequences(self.environment.value)
            response = requests.post(url=login_url, data=parameters)
            json_response = response.json()

            sequences = []
            if 'currentPageItems' in json_response:
                items = json_response['currentPageItems']
                for item in items:
                    sequence = OSCSequence.sequence_from_json(item)
                    sequences.append(sequence)

            return sequences, None
        except RequestException as ex:
            return None, ex

    def user_sequences(self, user_name: str) -> ([OSCSequence], Exception):
        """get all tracks for a user id """
        LOGGER.debug("getting all sequences for user: %s", user_name)
        try:
            parameters = {'ipp': 100,
                          'page': 1,
                          'username': user_name}
            json_response = requests.post(url=OSCApiMethods.user_sequences(self.environment.value),
                                          data=parameters).json()

            if 'totalFilteredItems' not in json_response:
                return [], Exception("OSC API bug missing totalFilteredItems from response")

            total_items = int(json_response['totalFilteredItems'][0])
            pages_count = int(total_items / parameters['ipp']) + 1
            LOGGER.debug("all sequences count: %s pages count: %s",
                         str(total_items), str(pages_count))
            sequences = []
            if 'currentPageItems' in json_response:
                for item in json_response['currentPageItems']:
                    sequences.append(OSCSequence.sequence_from_json(item))

            with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
                loop = asyncio.new_event_loop()
                futures = [
                    loop.run_in_executor(executor,
                                         self._sequence_page, user_name, page)
                    for page in range(2, pages_count + 1)
                ]
                if not futures:
                    loop.close()
                    return sequences, None

                done = loop.run_until_complete(asyncio.gather(*futures))
                loop.close()

                for sequence_page_return in done:
                    # sequence_page method will return a tuple the first element
                    # is a list of sequences
                    sequences = sequences + sequence_page_return[0]

                return sequences, None
        except RequestException as ex:
            return None, ex

    def sequence_details(self, sequence_id: str) -> OSCSequence:
        """method that returns the details of a sequence from OSC API"""
        try:
            parameters = {'id': sequence_id
                          }
            response = requests.post(OSCApiMethods.sequence_details(self.environment.value),
                                     data=parameters)
            json_response = response.json()
            if 'osv' in json_response:
                osc_data = json_response['osv']
                sequence = OSCSequence.sequence_from_json(osc_data)
                sequence.online_id = sequence_id
                return sequence
        except RequestException as ex:
            return ex

        return None

    def download_metadata(self, sequence: OSCSequence, path: str, override=False):
        """this method will download a metadata file of a sequence to the specified path.
        If there is a metadata file at that path by default no override will be made."""
        if sequence.metadata_url is None:
            return None
        metadata_path = path + "/metadata.txt"
        if not override and os.path.isfile(metadata_path):
            return None

        try:
            response = requests.get(OSCApiMethods.resource(self.environment.value,
                                                           sequence.metadata_url),
                                    stream=True)
            if response.status_code == 200:
                with open(metadata_path, 'wb') as file:
                    response.raw.decode_content = True
                    shutil.copyfileobj(response.raw, file)
        except RequestException as ex:
            return ex

        return None
