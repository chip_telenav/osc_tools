#!/usr/bin/python
"""This script is used to process files and communicate with OSC servers."""

import logging
import os
from argparse import ArgumentParser
from tqdm import tqdm
from osc_api_gateway import OSCApi
from osc_api_gateway import OSCAPIEnvironment


LOGGER = logging.getLogger('osc_tools')
OSC_LOG_FILE = 'OSC_logs.log'


def main():
    """Entry point for the script"""
    args = get_args()
    configure_log(args)
    # call the right sub command
    args.func(args)


def configure_api(args) -> OSCApi:
    """Method to configure upload environment"""
    if args.env == 'p':
        LOGGER.debug("environment production")
        api = OSCApi(OSCAPIEnvironment.PRODUCTION)
    elif args.env == 't':
        LOGGER.debug("environment testing")
        api = OSCApi(OSCAPIEnvironment.TESTING)
    elif args.env == 's':
        LOGGER.debug("environment staging")
        api = OSCApi(OSCAPIEnvironment.STAGING)
    elif args.env == 'b':
        LOGGER.debug("environment beta")
        api = OSCApi(OSCAPIEnvironment.BETA)
    else:
        LOGGER.debug("environment default production")
        api = OSCApi(OSCAPIEnvironment.PRODUCTION)
    return api


def configure_log(args):
    """Method to configure logging level"""
    LOGGER.setLevel(logging.DEBUG)

    # create file handler which logs even debug messages
    file = logging.FileHandler(OSC_LOG_FILE)
    file.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(name)-30s: %(levelname)-8s %(message)s')
    file.setFormatter(formatter)

    # create console handler with a higher log level
    console = logging.StreamHandler()
    console.setLevel(logging.WARNING)

    if args.log_level == 'd':
        console.setLevel(logging.DEBUG)
        console.setFormatter(formatter)
        LOGGER.debug("Debug logging selected")
    elif args.log_level == 'i':
        console.setLevel(logging.INFO)
        formatter = logging.Formatter('%(message)s')
        console.setFormatter(formatter)
        LOGGER.debug("info logging selected")
    elif args.log_level == 'w':
        console.setLevel(logging.WARNING)
        formatter = logging.Formatter('%(message)s')
        console.setFormatter(formatter)
        LOGGER.debug("Warning logging selected")

    # add the handlers to the logger
    LOGGER.addHandler(file)
    LOGGER.addHandler(console)


def download_command(args):
    """download command"""
    # path where to download the selected tracks
    path = args.path
    override = args.override
    workers = args.workers

    if args.ids is None and args.username is None:
        LOGGER.warning("missing arguments ids and username. At least "
                       "one of them is required to start a download.")
    elif args.ids is not None and args.username is not None:
        LOGGER.warning("ids and username are both set but they are mutually exclusie.")
    elif args.ids is not None:
        LOGGER.warning("downloading using ids")
        ids = args.ids
        osc_api = configure_api(args)
        for i in tqdm(range(len(ids))):
            sequence_id = ids[i]
            LOGGER.debug("getting photo links for sequence: %s", str(sequence_id))
            sequence = osc_api.sequence_details(sequence_id)
            photo_list, _ = osc_api.get_photos(sequence_id)
            track_path = path + '/' + str(sequence_id)
            if not os.path.exists(track_path):
                os.mkdir(track_path)
            osc_api.download_metadata(sequence,
                                      track_path,
                                      override)
            osc_api.download_all_images(photo_list,
                                        track_path,
                                        override,
                                        workers)
    else:
        user_name = args.username
        LOGGER.warning("downloading using username: %s", user_name)
        osc_api = configure_api(args)
        sequences, _ = osc_api.user_sequences(user_name)

        user_path = path + "/" + user_name
        if not os.path.exists(user_path):
            os.mkdir(user_path)

        LOGGER.debug("user sequences to download: %s", str(len(sequences)))
        for i in tqdm(range(len(sequences))):
            sequence = sequences[i]
            LOGGER.debug("getting photo links for sequence: %s", str(sequence.online_id))
            photo_list, _ = osc_api.get_photos(sequence.online_id)
            track_path = user_path + '/' + str(sequence.online_id)
            LOGGER.debug("starting to download for sequence: %s this sequence has: %s photos",
                         str(sequence.online_id), str(len(photo_list)))
            if not os.path.exists(track_path):
                os.mkdir(track_path)
            osc_api.download_metadata(sequence,
                                      track_path,
                                      override)
            osc_api.download_all_images(photo_list,
                                        track_path,
                                        override,
                                        workers)


def get_args() -> list:
    """Method to create and configure a argument parser"""
    parser: ArgumentParser = ArgumentParser(prog='OSC tools',
                                            description='This script will download all the '
                                                        'files form the path sent as '
                                                        'argument.')
    parser.add_argument('-l',
                        '--log_level',
                        required=False,
                        default='i',
                        help='Specify level of logging to console.'
                             'd level (debug) will log every event to the console'
                             'i level (info) will log every event more severe than '
                             'debug level to the console'
                             'w level (warning) will log every event more severe than '
                             'info level to the console',
                        choices=['d', 'i', 'w'])
    parser.add_argument('-e',
                        '--env',
                        default='p',
                        required=False,
                        help='Specify the web environment to communicate with.'
                             'p for Production'
                             't for Testing'
                             's for Staging '
                             'b for Beta',
                        choices=['p', 't', 's', 'b'])

    subparsers = parser.add_subparsers(title='Sub commands',
                                       description='this are the available commands',
                                       help='Download sub command.',
                                       dest='sub command')
    subparsers.required = True

    download_parser = subparsers.add_parser('download')
    download_parser.set_defaults(func=download_command)
    download_parser.add_argument('-p',
                                 '--path',
                                 required=True,
                                 help='Full path directory where to download the sequences')
    download_parser.add_argument('-ids',
                                 '--ids',
                                 required=False,
                                 nargs='+',
                                 help='List of sequence ids to download photos')
    download_parser.add_argument('-u',
                                 '--username',
                                 required=False,
                                 help='A user name from where to download photos')
    download_parser.add_argument('-o',
                                 '--override',
                                 required=False,
                                 action='store_true',
                                 help='Flag to specify if previous progress should be ignored '
                                      'and download should start from scratch.')
    download_parser.add_argument('-w',
                                 '--workers',
                                 required=False,
                                 type=int,
                                 default=10,
                                 choices=range(1, 21),
                                 help='Integer value to determine de workers used to '
                                      'download images. Default value is 10')

    return parser.parse_args()


if __name__ == "__main__":
    main()
