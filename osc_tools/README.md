![OSC](http://openstreetcam.org/assets/images/osc-logo.png)

## Download photos taken with OSC apps

##### Description
This script is used to download photos from [OpenStreetCam](https://www.openstreetcam.org/). 


##### Requirements
* Python 3  
* Dependencies from _requirements.txt_ file.
The dependencies can be installed by running:
```
pip3 install virtualenv

virtualenv -p python3 .

source bin/activate

pip3 install -r requirements.txt
```

##### Usage

```
cd /path_to_scripts

# help
python osc_tools.py -h

# help for download
python osc_tools.py download -h 

# download several sequences, knowing their ids are 101, 12 and 43521 to ~/Downloads/OSC_sequences folder 
python osc_tools.py download -p ~/Downloads/OSC_sequences -ids 101 12 43521

# download all sequences of a certain user to ~/Downloads/OSC_sequences folder
python osc_tools.py download -p ~/Downloads/OSC_sequences -u UserName

```    